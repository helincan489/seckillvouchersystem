package com.example.hexapplication.Application;

import com.example.hexapplication.Application.pojo.voucher;
import com.example.hexapplication.Application.port.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;

import static com.example.hexapplication.Application.util.cacheconstant.*;
import static org.mockito.Mockito.*;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application
 * @className: voucherServiceUnitTest
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 6:38
 * @version: 1.0
 */

@SpringBootTest
@Slf4j
public class voucherServiceUnitTest {
    @Autowired
    voucherService voucherService;

    @MockBean
    voucherMapper voucherMapper;

    @MockBean
    cache cache;

    @MockBean
    mq mq;

    @Test
    public void VoucherTimeInValid()
    {
        LocalDateTime startTime=LocalDateTime.of(2024,2,14,0,0);
        LocalDateTime endTime=LocalDateTime.of(2024,2,15,0,0);
        Long voucherId=1L;
        when(cache.valueOfHash(VOUCHER_LIFE_PREFIX+voucherId,START_TIME)).thenReturn(String.valueOf(startTime));
        when(cache.valueOfHash(VOUCHER_LIFE_PREFIX+voucherId,END_TIME)).thenReturn(String.valueOf(endTime));
        boolean result=voucherService.ifTimeValid(LocalDateTime.of(2024,2,13,0,0),voucherId);
        Assertions.assertFalse(result);
    }

    @Test
    public void VoucherTimeValid()
    {
        LocalDateTime startTime=LocalDateTime.of(2024,2,14,0,0);
        LocalDateTime endTime=LocalDateTime.of(2024,2,16,0,0);
        Long voucherId=1L;
        when(cache.valueOfHash(VOUCHER_LIFE_PREFIX+voucherId,START_TIME)).thenReturn(String.valueOf(startTime));
        when(cache.valueOfHash(VOUCHER_LIFE_PREFIX+voucherId,END_TIME)).thenReturn(String.valueOf(endTime));
       boolean result=voucherService.ifTimeValid(LocalDateTime.of(2024,2,14,5,0),voucherId);
       Assertions.assertTrue(result);
    }

    @Test
    public void addNewVoucher()
    {
        voucher voucher1= voucher.builder().voucherId(1L).stock(2).beginTime(LocalDateTime.of(2024,1,1,0,0))
                        .endTime(LocalDateTime.of(2024,1,2,0,0)).build();
        voucherService.addVoucher(voucher1);
        verify(voucherMapper,times(1)).save(voucher1);
        verify(cache,times(1)).setHash(VOUCHER_LIFE_PREFIX+voucher1.getVoucherId(),START_TIME,String.valueOf(voucher1.getBeginTime()));
        verify(cache,times(1)).setHash(VOUCHER_LIFE_PREFIX+voucher1.getVoucherId(),END_TIME,String.valueOf(voucher1.getEndTime()));
    }
}
