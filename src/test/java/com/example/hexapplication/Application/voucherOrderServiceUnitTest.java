package com.example.hexapplication.Application;

import com.example.hexapplication.Application.pojo.voucherOrder;
import com.example.hexapplication.Application.port.*;
import com.example.hexapplication.Application.port.voucherOrderService;
import com.github.f4b6a3.uuid.UuidCreator;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;

import static com.example.hexapplication.Application.util.mqconstant.exchanger_name;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application
 * @className: voucherOrderServiceUnitTest
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 5:07
 * @version: 1.0
 */

@SpringBootTest
@Slf4j
public class voucherOrderServiceUnitTest {

    @Autowired
    voucherOrderService voucherOrderService;

    @MockBean
    cache cache;

    @MockBean
    mq mq;

    @MockBean
    voucherService voucherService;

    @Test
    public void voucherCanNotBeOrderedNow()
    {
        Long voucherId=1L;
        Long userId=1L;
        Long orderId= UuidCreator.getTimeBased().timestamp();
        LocalDateTime localDateTime=LocalDateTime.now();
        when(voucherService.ifTimeValid(localDateTime,voucherId)).thenReturn(false);
        String result=voucherOrderService.addVoucherOrder(voucherId,userId,localDateTime,orderId);
        Assertions.assertEquals(result,"voucher can not be ordered now");
    }

    @Test
    public void voucherHasBeenOrderedByThisUser()
    {
        Long voucherId=1L;
        Long userId=1L;
        Long orderId= UuidCreator.getTimeBased().timestamp();
        LocalDateTime localDateTime=LocalDateTime.now();
        when(voucherService.ifTimeValid(localDateTime,voucherId)).thenReturn(true);
        when(cache.ifOrderValid(voucherId,userId)).thenReturn(2);
        String result=voucherOrderService.addVoucherOrder(voucherId,userId,localDateTime,orderId);
        Assertions.assertEquals(result,"This voucher is already grabbed");
    }

    @Test
    public void voucherHasNoEnoughStock()
    {
        Long voucherId=1L;
        Long userId=1L;
        Long orderId= UuidCreator.getTimeBased().timestamp();
        LocalDateTime localDateTime=LocalDateTime.now();
        when(voucherService.ifTimeValid(localDateTime,voucherId)).thenReturn(true);
        when(cache.ifOrderValid(voucherId,userId)).thenReturn(1);
        String result=voucherOrderService.addVoucherOrder(voucherId,userId,localDateTime,orderId);
        Assertions.assertEquals(result,"There is no voucher left");
    }

    @Test
    public void addVoucherSuccessfully()
    {
        Long voucherId=1L;
        Long userId=1L;
        Long orderId= UuidCreator.getTimeBased().timestamp();
        LocalDateTime localDateTime=LocalDateTime.now();
        when(voucherService.ifTimeValid(localDateTime,voucherId)).thenReturn(true);
        when(cache.ifOrderValid(voucherId,userId)).thenReturn(0);
        String result=voucherOrderService.addVoucherOrder(voucherId,userId,localDateTime,orderId);
        verify(mq).sentToQueue(eq(exchanger_name),any(voucherOrder.class));
        Assertions.assertEquals(result,"voucher is ordered successfully");
    }

}
