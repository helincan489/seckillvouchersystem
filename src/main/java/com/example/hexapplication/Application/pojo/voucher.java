package com.example.hexapplication.Application.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application.pojo
 * @className: voucher
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 6:19
 * @version: 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class voucher {
    private Long voucherId;
    private Integer stock;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private LocalDateTime beginTime;
    private LocalDateTime endTime;
}
