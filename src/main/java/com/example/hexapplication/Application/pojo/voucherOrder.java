package com.example.hexapplication.Application.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application.pojo
 * @className: voucherOrder
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 4:48
 * @version: 1.0
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class voucherOrder {

    private Long id;
    private Long userId;
    private Long voucherId;
    private Integer status;
    private LocalDateTime createTime;
    private LocalDateTime payTime;
}
