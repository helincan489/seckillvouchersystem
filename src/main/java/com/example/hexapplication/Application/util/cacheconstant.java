package com.example.hexapplication.Application.util;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application.util
 * @className: cacheconstant
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 6:26
 * @version: 1.0
 */
public class cacheconstant {
    public static String VOUCHER_PREFIX="";
    public static String VOUCHER_LIFE_PREFIX="";

    public static String START_TIME="voucher start time: ";

    public static String END_TIME="voucher end time: ";
}
