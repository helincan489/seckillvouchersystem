package com.example.hexapplication.Application;

import com.example.hexapplication.Application.pojo.voucherOrder;
import com.example.hexapplication.Application.port.cache;
import com.example.hexapplication.Application.port.mq;
import com.example.hexapplication.Application.port.voucherOrderService;
import com.example.hexapplication.Application.port.voucherService;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.github.f4b6a3.uuid.UuidCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

import static com.example.hexapplication.Application.util.mqconstant.exchanger_name;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application.impl
 * @className: voucherOrderServiceImpl
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 4:34
 * @version: 1.0
 */

@Service
public class voucherOrderServiceImpl implements voucherOrderService {

    @Autowired
    private cache cache;

    @Autowired
    private mq mq;

    @Autowired
    private voucherService voucherService;


    @Override
    public String addVoucherOrder(Long voucherId, Long userId,LocalDateTime localDateTime,Long orderId) {
        if (!voucherService.ifTimeValid(localDateTime,voucherId))
        {
            return "voucher can not be ordered now";
        }
        Integer result=cache.ifOrderValid(voucherId,userId);
        if (result !=0)
        {
            return result ==1 ? "There is no voucher left":"This voucher is already grabbed";
        }
        voucherOrder order=voucherOrder.builder().voucherId(voucherId).userId(userId).id(orderId).status(0).createTime(LocalDateTime.now()).
                build();
        mq.sentToQueue(exchanger_name,order);
        return "voucher is ordered successfully";
    }

}
