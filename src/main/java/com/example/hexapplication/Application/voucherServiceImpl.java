package com.example.hexapplication.Application;

import com.example.hexapplication.Application.pojo.voucher;
import com.example.hexapplication.Application.port.cache;
import com.example.hexapplication.Application.port.voucherMapper;
import com.example.hexapplication.Application.port.voucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.example.hexapplication.Application.util.cacheconstant.*;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application
 * @className: voucherServiceImpl
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 6:18
 * @version: 1.0
 */

@Service
public class voucherServiceImpl implements voucherService {

    @Autowired
    voucherMapper voucherMapper;

    @Autowired
    cache cache;

    @Override
    public boolean ifTimeValid(LocalDateTime curTime, Long voucherId) {
        LocalDateTime startTime=LocalDateTime.parse(cache.valueOfHash(VOUCHER_LIFE_PREFIX+voucherId,START_TIME));
        LocalDateTime endTime=LocalDateTime.parse(cache.valueOfHash(VOUCHER_LIFE_PREFIX+voucherId,END_TIME));
        if (curTime.isAfter(endTime) || curTime.isBefore(startTime)) return false;
        return true;
    }

    @Override
    public void addVoucher(voucher voucher) {
        voucherMapper.save(voucher);
        cache.set(VOUCHER_PREFIX+voucher.getVoucherId(),voucher.getStock().toString());
        cache.setHash(VOUCHER_LIFE_PREFIX+voucher.getVoucherId(),START_TIME,String.valueOf(voucher.getBeginTime()));
        cache.setHash(VOUCHER_LIFE_PREFIX+voucher.getVoucherId(),END_TIME,String.valueOf(voucher.getEndTime()));
    }
}
