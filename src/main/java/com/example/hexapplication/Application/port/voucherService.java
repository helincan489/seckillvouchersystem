package com.example.hexapplication.Application.port;


import com.example.hexapplication.Application.pojo.voucher;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public interface voucherService {
    boolean ifTimeValid(LocalDateTime curTime,Long voucherId);

    void addVoucher(voucher voucher);
}
