package com.example.hexapplication.Application.port;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public interface voucherOrderService {
    String addVoucherOrder(Long voucherId, Long userId, LocalDateTime localDateTime, Long orderId);
}
