package com.example.hexapplication.Application.port;


import com.example.hexapplication.Application.pojo.voucher;
import org.springframework.stereotype.Component;

/**
 * @projectName: HexApplication
 * @package: com.example.hexapplication.Application.port
 * @className: voucherMapper
 * @author: Lincan
 * @description: TODO
 * @date: 2024/2/13 6:22
 * @version: 1.0
 */

@Component
public interface voucherMapper {
    void save(voucher voucher);
}
