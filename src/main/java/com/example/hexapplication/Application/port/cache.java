package com.example.hexapplication.Application.port;


import org.springframework.stereotype.Component;

@Component
public interface cache {
    Integer ifOrderValid(Long voucherId, Long userId);
    void set(String key, String value);

    void setHash(String key1, String key2, String value);

    String valueOfHash(String key1, String key2);
}
