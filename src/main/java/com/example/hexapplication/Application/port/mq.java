package com.example.hexapplication.Application.port;

import com.example.hexapplication.Application.pojo.voucherOrder;
import org.springframework.stereotype.Component;

@Component
public interface mq {
    void sentToQueue(String exchangerName, voucherOrder order);
}
