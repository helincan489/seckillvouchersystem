package com.example.hexapplication.adapter;

import com.example.hexapplication.Application.port.cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;


@Component
public class RedisAdapter implements cache {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Override
    public Integer ifOrderValid(Long voucherId, Long userId) {
        return null;
    }

    @Override
    public void set(String key, String value) {
        stringRedisTemplate.opsForValue().set(key,value);
    }

    @Override
    public void setHash(String key1, String key2, String value) {
        stringRedisTemplate.opsForHash().put(key1,key2,value);
    }

    @Override
    public String valueOfHash(String key1, String key2) {
        return (String) stringRedisTemplate.opsForHash().get(key1,key2);
    }
}
